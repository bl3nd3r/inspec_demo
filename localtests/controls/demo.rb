title "sample section"

control "basic-os" do                        # A unique ID for this control
  impact 0.7                                # The criticality, if this control fails.
  title "Some pre-requisites for the machine we're running on"             # A human-readable title
  desc "Check that the operating system is of the right type and able to run our tests."

  describe os.linux? do                  # The actual test
    it { should be true }
  end

  describe user('inspec') do
    it { should exist }
    its('uid') { should eq 1000 }
  end

  # Use plain old ruby anywhere you like
  [ 'git', 'openssh-client'].each { |apt|
    describe package(apt) do
      it { should be_installed }
    end
  }

  # rubocop isn't installed. That one test will fail, the other two will pass.
  [ 'inspec', 'inspec-bin', 'rubocop'].each { |gem|
    describe gem(gem) do
      it { should be_installed }
      its('versions.count') { should eq 1 }
    end
  }
end
